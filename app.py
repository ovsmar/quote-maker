from flask import Flask, render_template, request, flash, redirect, url_for ,send_file, Response,session
import os
import quote
from decouple import config
from base64 import b64encode
import io
from PIL import Image
import base64
import uuid
import pathlib

app = Flask(__name__)
app.config['SECRET_KEY'] = config('KEY')

@app.route('/')
def redirection():
    return redirect('QuoteMaker')


@app.route('/QuoteMaker', methods=['GET', 'POST'])
def QuoteMaker():
    global id
    id = session["user_name"] = str(uuid.uuid4())
    print(session)
    global quoteID
    if request.form.get('createquaote'):
       
        quoteID = str(uuid.uuid4())
        quoteSTR = request.form['entry_quote']
        tagSTR = request.form['entry_tag']
        color = request.form['color']
    
        quote.quote_main(quoteSTR,tagSTR,quoteID,id,color)
        
        im = Image.open(f"pack2/{quoteID+id}.png")
        data = io.BytesIO()
        im.save(data, "PNG")
        encoded_img_data = base64.b64encode(data.getvalue())
        
        flash('Successfully created!')      
        return render_template("QuoteMaker.html", img_data=encoded_img_data.decode('utf-8'))
    else:
        return render_template("QuoteMaker.html") 


def deleteIMG():
    global quoteID
    global id
    if os.path.isfile(f"pack2/{quoteID+id}.png"):
            os.unlink(f"pack2/{quoteID+id}.png")
    else:
        print("non") 


@app.route('/dowlandMaker', methods=['GET', 'POST'])
def dowlandMaker():
    global quoteID
    if request.form.get('dowland'):
        if 'quoteID' in globals():
            if os.path.isfile(f"pack2/{quoteID+id}.png"):
                try:
                    open(f"pack2/{quoteID+id}.png")
                    return send_file(f"pack2/{quoteID+id}.png", as_attachment=True,mimetype='image/png',download_name="quote.png") , deleteIMG()
                except IOError as e:
                    pass 
            else:
                flash("Quote no longer exists, please recreate.")
                return render_template("QuoteMaker.html") 
        else:
            flash("Quote no longer exists, please recreate.")
            return render_template("QuoteMaker.html")
    else:
        return render_template("QuoteMaker.html")  



