let img = document.getElementById('picture');
let example = document.getElementById('example');


let src = img.getAttribute('src');
console.log(src)

if (src.indexOf('+') > -1) {
    img.style.visibility = 'visible';
    example.style.display = 'none';

   
} else {
    img.style.visibility = 'collapse';
    example.style.display = 'visible';
    
}

function togglePopup(){
    document.getElementById("popup-1").classList.toggle("active");
  }


window.setTimeout(function(){ 
	// First check, if localStorage is supported.
	if (window.localStorage) {
		// Get the expiration date of the previous popup.
		var nextPopup = localStorage.getItem( 'nextNewsletter' );
		console.log(nextPopup)

		if (nextPopup > new Date()) {
			return;
		}

		// Store the expiration date of the current popup in localStorage.
		var expires = new Date();
		expires = expires.setHours(expires.getHours() + 24);

		localStorage.setItem( 'nextNewsletter', expires );
	}

	document.getElementById("popup-1").classList.toggle("active");
}, 1000);


// mode 
function toggle_light_mode() {
	var app = document.getElementsByTagName("BODY")[0];
	if (localStorage.lightMode == "dark") {
		localStorage.lightMode = "light";
		app.setAttribute("light-mode", "light");
	} else {
		localStorage.lightMode = "dark";
		app.setAttribute("light-mode", "dark");
	}       
}

window.addEventListener("storage", function () {
	if (localStorage.lightMode == "dark") {
		app.setAttribute("light-mode", "dark");
	} else {
		app.setAttribute("light-mode", "light");
	}
}, false);