# Quote Maker

This is simple Quote Maker with the ability to change color of background and download it (python/flask/pillow/javascipt/html/css) 

Le projet Quote Maker est un outil qui permet créer des citations avec un arrière-plan de couleur personnalisable et de les télécharger sous forme d'image. Il a été développé en utilisant les technologies Python, Flask, Pillow - la bibliothèque pour traiter les images, JavaScript, HTML et CSS. Il offre une interface simple qui permet aux utilisateurs de saisir une citation, ajouter un tag et choisir une couleur de fond. Une fois la citation créée, l'utilisateur peut télécharger l'image résultante sur son ordinateur.

![view](static/v.png)
![view](static/quote.png)
