from PIL import Image, ImageDraw, ImageFont,ImageOps
import textwrap
from pathlib import Path
import os.path
import uuid



def quote_main(quoteSTR,tagSTR,quoteID,id,color):
    para = textwrap.wrap(quoteSTR, width=23)
    para2 = textwrap.wrap(tagSTR, width=15)


    
    MAX_W, MAX_H = 510, 510
    im = Image.new('RGB', (MAX_W, MAX_H), (f"{color}"))
    draw = ImageDraw.Draw(im)
    
    font = ImageFont.truetype("Pillow/Tests/fonts/FreeMono.ttf", 35)
    font2 = ImageFont.truetype("Pillow/Tests/fonts/FreeMono.ttf", 22)


    current_h, pad = 200, 10
    for line in para:
        w, h = draw.textsize(line, font=font)
        draw.text(((MAX_W - w) / 2, current_h), line, font=font,fill=(0,0,0))
        current_h += h + pad


    current_h2, pad2 = 460, 10
    for line in para2:
        w, h = draw.textsize(line, font=font2)
        draw.text(((MAX_W - w) / 2, current_h2), line, font=font2,fill=(0,0,0))
        current_h2 += h + pad2


    im.save(f"pack/{quoteID}.png")

    if os.path.isfile(f"pack/{quoteID}.png"):
 
        ImageOps.expand(Image.open(f"pack/{quoteID}.png"),border=1,fill='black').save(f"pack2/{quoteID+id}.png")

        [f.unlink() for f in Path("pack").glob("*") if f.is_file()]
    else:
        print("non")

